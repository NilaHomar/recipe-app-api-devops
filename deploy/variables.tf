variable "prefix" {
  default = "recipe-app"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "nilahomar@gmail.com"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "ami" {
  default = "ami-01be94ae58414ab2e"
}
