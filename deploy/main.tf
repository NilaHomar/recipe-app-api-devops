terraform {
  backend "s3" {
    bucket         = "nila-recipe-app-tfstate"
    key            = "recipe-app-api-devops/deploy/terraform.tfstate"
    region         = "eu-central-1"
    encrypt        = true
    dynamodb_table = "nila-recipe-app-tfstate"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~>5.0"
    }
  }
}
provider "aws" {
  region = "eu-central-1"
}
locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

data "aws_region" "current" {}
