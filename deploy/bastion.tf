data "aws_ami" "amazon_linux" {
  most_recent = true
  filter {
    name   = "name"
    values = ["al2023-ami-2023.*-x86_64"]
  }
  owners = ["amazon"]
}
resource "aws_instance" "bastion" {
  ami           = var.ami
  instance_type = var.instance_type
  tags = merge(
    local.common_tags,
    {
      Name = "${local.prefix}-bastion"
    }
  )
}
